# Copyright 2008 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Purpose License v2

require gedit vala [ with_opt=true vala_dep=true ]
require python [ blacklist=2 multibuild=false ]

LICENCES="GPL-2"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="gtk-doc python spell
    gvfs-metadata  [[ description = [ use gvfs to store metadata ] ]]
    ( linguas: af am an ar as ast az be be@latin bg bn bn_IN br bs ca ca@valencia crh cs cy da de dz
               el en_CA en_GB en@shaw eo es et eu fa fi fr ga gd gl gu he hi hr hu hy id is it ja ka
               kk km kn ko ku la lt lv mai mg mi mk ml mn mr ms my nb nds ne nl nn oc or pa pl ps pt
               pt_BR ro ru rw si sk sl sq sr sr@latin sv ta te tg th tk tr ug uk vi wa xh zh_CN zh_HK
               zh_TW )
"

DEPENDENCIES="
    build:
        dev-util/intltool[>=0.50.1]
        dev-util/itstool [[ note = [ from YELP_HELP_INIT in configure.ac ] ]]
        gnome-desktop/yelp-tools
        sys-devel/gettext[>=0.18]
        virtual/pkg-config[>=0.20]
        gtk-doc? ( dev-doc/gtk-doc[>=1.0] )
    build+run:
        dev-libs/atk
        dev-libs/glib:2[>=2.44.0]
        dev-libs/libpeas:1.0[>=1.14.1]
        dev-libs/libxml2:2.0[>=2.5.0]
        gnome-desktop/gobject-introspection:1[>=0.9.3]
        gnome-desktop/gsettings-desktop-schemas
        gnome-desktop/gtksourceview:3.0[>=3.22.0][gobject-introspection]
        x11-libs/cairo
        x11-libs/gdk-pixbuf
        x11-libs/gtk+:3[>=3.21.3][gobject-introspection]
        x11-libs/libICE
        x11-libs/libSM[>=1.0.0]
        x11-libs/libX11
        x11-libs/pango
        gvfs-metadata? ( gnome-desktop/gvfs )
        python? ( gnome-bindings/pygobject:3[>=3.0.0][python_abis:*(-)?] )
        spell? ( gnome-desktop/gspell:1[>=0.2.5] )
    suggestion:
        gnome-desktop/gedit-plugins [[ description = [ Plugins for additional features ] ]]
"

RESTRICT="test" # uses X

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --exec-prefix=/usr/$(exhost --target)
    --disable-updater
    --enable-introspection
    --disable-schemas-compile
)

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( gtk-doc python spell 'vapi vala' gvfs-metadata )

