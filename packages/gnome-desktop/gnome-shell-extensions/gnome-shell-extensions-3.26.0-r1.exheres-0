# Copyright 2011 Brett Witherspoon <spoonb@exherbo.org>
# Copyright 2013 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

declare -A CLASSIC_EXTENSIONS BUILTIN_EXTENSIONS EXTERNAL_EXTENSIONS

CLASSIC_EXTENSIONS=(
    [apps-menu]='GNOME 2 style menu for applications'
    [alternate-tab]='A replacement for Alt-Tab, allows to cycle between windows and does not group by application'
    [launch-new-instance]='Launch new instance'
    [places-menu]='Status menu for quickly navigating places in the system'
    [window-list]='Display a window list at the bottom of the screen'
)
BUILTIN_EXTENSIONS=(
    [drive-menu]='A status menu for accessing and unmounting removable devices'
    [screenshot-window-sizer]='Screenshot Window Sizer for Gnome Shell'
    [windowsNavigator]='Allow keyboard selection of windows and workspaces in overlay mode'
    [workspace-indicator]='Indicator in which workspace you are and give you the possibility of switching to another one'
)
EXTERNAL_EXTENSIONS=(
    [auto-move-windows]='Move applications to specific workspaces when they create windows'
    [native-window-placement]='Arrange windows in overview in a more native way'
    [user-theme]='Load shell themes from user directory'
)

require gnome.org [ suffix=tar.xz ]
require gnome-shell-extensions [ extensions=[ "${!CLASSIC_EXTENSIONS[@]}" "${!BUILTIN_EXTENSIONS[@]}" "${!EXTERNAL_EXTENSIONS[@]}" ] extensions_desc=[ "${CLASSIC_EXTENSIONS[@]}" "${BUILTIN_EXTENSIONS[@]}" "${EXTERNAL_EXTENSIONS[@]}" ] ]

SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="classic-mode [[ description = [ enable plugins required for GNOME Classic Mode ] ]]
    (
        linguas:
            af an ar as be bg bn_IN bs ca ca@valencia cs da de el en_GB eo es et eu fa fi fr fur gd
            gl gu he hi hr hu id is it ja kk km kn ko lt lv ml mr ms nb ne nl oc or pa pl pt_BR pt
            ro ru sk sl sr@latin sr sv ta te tg th tr uk vi zh_CN zh_HK zh_TW
    )
"

DEPENDENCIES="
    build:
        sys-devel/gettext[>=0.19.6]
        virtual/pkg-config[>=0.22]
    build+run:
        dev-libs/glib:2[>=2.26] [[ note = [ For GSettings ] ]]
    run:
        gnome-desktop/gnome-shell[=$(ever range 1-2)*]
        apps-menu? ( gnome-desktop/gnome-menus:3.0[gobject-introspection] )
        auto-move-windows? ( gnome-desktop/gnome-menus:3.0[gobject-introspection] )
    suggestion:
        gnome-desktop/gnome-tweaks [[ description = [ Enable/disable extensions ] ]]
"

DEFAULT_SRC_CONFIGURE_OPTION_ENABLES=( 'classic-mode' )

