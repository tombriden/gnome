# Copyright 2009 Saleem Abdulrasool <compnerd@compnerd.org>
# Distributed under the terms of the GNU General Public License v2

require gjs

LICENCES="MIT || ( MPL-1.1 LGPL-2 GPL-2 )"
SLOT="1"
PLATFORMS="~amd64 ~x86"
MYOPTIONS="
    gtk   [[ description = [ GTK+ support ] ]]
"

DEPENDENCIES="
    build:
        sys-devel/gettext
        virtual/pkg-config[>=0.20]
    build+run:
        gnome-desktop/gobject-introspection[>=1.53.4]
        dev-libs/libffi
        dev-libs/glib:2[>=2.50.0]
        dev-libs/spidermonkey:52
        sys-libs/readline:=
        x11-libs/cairo
        gtk? ( x11-libs/gtk+:3[>=3.20][gobject-introspection] )
    test:
        dev-lang/python:* [[ note = [ required to build tests ] ]]
"

DEFAULT_SRC_CONFIGURE_PARAMS=(
    --prefix=/usr
    --exec_prefix=/usr/$(exhost --target)
    --includedir=/usr/$(exhost --target)/include
    --with-cairo
    --enable-asan
    --enable-ubsan
    --disable-systemtap
    --disable-dtrace
)

DEFAULT_SRC_CONFIGURE_OPTION_WITHS=(
    gtk
)

src_prepare() {
    # disable tests which require X
    edo sed -e "s:installed-tests/js/testCairo.js::" -i \
        Makefile.in
    edo sed -e "s:installed-tests/js/testGtk.js::" -i \
        Makefile.in
    edo sed -e "s:installed-tests/js/testGObjectDestructionAccess.js::" -i \
        Makefile.in
    edo sed -e "s:installed-tests/js/testLegacyGtk.js::" -i \
        Makefile.in
    edo sed -e "s:installed-tests/js/testLegacyGObject.js::" -i \
        Makefile.in

    # Test fails without gtk:
    # Gjs-WARNING **: JS ERROR: Error: Requiring Gtk
    if ! optionq gtk; then
        edo sed -e "s:installed-tests/js/testGObjectClass.js::" -i \
            Makefile.in
    fi
}

